package com.java.jpa.models;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Manjunath
 *
 */
@Entity
@Table(name = "LOCATIONS")
public class Location implements Serializable{
	//private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long id;

    String houseno ;

    String sreetname;

    String postcode;

    String city;
    String country;

	//public String SEQUENCE_NAME;
    
	//@Column
	//private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getHouseno() {
		return houseno;
	}

	public void setHouseno(String houseno) {
		this.houseno = houseno;
	}
    

    public String getStreetName() {
        return sreetname;
    }

    public void setStreetName(String sreetname) {
        this.sreetname = sreetname;
    }

    public String getPostCode() {
        return postcode;
    }

    public void setPostCode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }
    
    public void setCity(String city) {
        this.city=city;
    }

  

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
   /* public Date getcreateDate() {
		return createDate;
	}

	public void setcreateDate(Date createDate) {
		this.createDate = createDate;
	}*/
	

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", houseno='" + getHouseno() + '\'' +
                ", streetname='" + sreetname + '\'' +
                ", postcode='" + postcode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) { return true; }
        if (!(o instanceof Location)) { return false; }
        final Location location = (Location) o;
        return Objects.equals(getId(), location.getId()) &&
        		Objects.deepEquals(getHouseno(), location.getHouseno()) &&
                Objects.equals(getStreetName(), location.getStreetName()) &&
                Objects.equals(getPostCode(), location.getPostCode()) &&
                Objects.equals(getCity(), location.getCity()) &&
                Objects.equals(getCountry(), location.getCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getHouseno(),getStreetName(), getPostCode(), getCity(), getCountry());
    }

	
}
