package com.java.jpa.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.jpa.models.Location;

@Transactional
public interface LocationRepository extends CrudRepository<Location, Long> {
	
	
}

