package com.java.jpa.controller;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.jpa.models.Location;
import com.java.jpa.repository.LocationRepository;



@RestController
@EnableAutoConfiguration
@RequestMapping("/api/v1")
public class LocationController {
	//@Autowired
	//LocationServiceFacade locationservicefacade;

	@Autowired
	LocationRepository locationRepository;
	/**
	 * GET /create  --> Create a new customer and save it in the database.
	 */
	@RequestMapping("/create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	public Location create(Location location) {
		//customer.setcreateDate(new Date());
		location = locationRepository.save(location);
		//System.out.println(customer.getcaddress());
	return location;
	}
	
	/**
	 * GET /read  --> Read a address by id from the database.
	 */
	
	
	@RequestMapping(
			  value = "/read",
			   method = RequestMethod.GET)
	public Location readbyid(@RequestParam Long id) {
		Location location = locationRepository.findOne(id);
		
	    return location;
	}
	
/* * GET /read  --> Read a address more elements from the database.
	 */
	
	@RequestMapping(value="/params", method = RequestMethod.GET)
	public ResponseEntity getParams(@RequestParam Map<String, String> params ) {
    // Location location = locationRepository.findAll(params)
		
	   System.out.println(params.keySet());
	   System.out.println(params.values());

	   return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	/**
	 * POST /update  --> Update a customer record and save it in the database.
	 */
	/*@RequestMapping(
			  value = "/update", 
			  produces = "application/json", 
			  method = {RequestMethod.GET, RequestMethod.PUT})*/
	@RequestMapping("/update")
	public Location update(@RequestParam Long id,String houseno) {
		Location location = locationRepository.findOne(id);
		location.setHouseno(houseno);
		location = locationRepository.save(location);
	    return location;
	}
	
	/**
	 * GET /delete  --> Delete a booking from the database.
	 */
	@RequestMapping(
			  value = "/delete",
			   method = RequestMethod.GET)
	public String delete(@RequestParam Long id) {
		locationRepository.delete(id);
	    return "address #"+id+" deleted successfully";
	}
	
	@RequestMapping("/readall")
	public Iterable<Location> readAll() {
		Iterable<Location> customer =  locationRepository.findAll();
	    return customer;
	}
}
