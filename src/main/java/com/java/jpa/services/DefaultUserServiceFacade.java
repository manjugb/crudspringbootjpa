/*package com.java.jpa.services;

import java.util.List;
import java.util.Optional;

import com.java.jpa.models.Location;
import com.java.jpa.repository.LocationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DefaultUserServiceFacade implements LocationServiceFacade {
    @Autowired
    LocationRepository locationRepository;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Override
    public Location createLocation(final Location Location) {
        Location.setId(sequenceGeneratorService.generateSequence(Location.SEQUENCE_NAME));
        return locationRepository.save(Location);
    }

    @Override
    public List<Location> readAllLocaiton() {
        return (List<Location>) locationRepository.findAll();
    }

    @Override
    public Location findLocationById(final Long id) {
        Optional<Location> LocationOptional = locationRepository.findOne(id);
        if (LocationOptional.isPresent()) {
            return LocationOptional.get();
        } else {
            throw new RuntimeException("User not found with id " + id);
        }
    }

    @Override
    public Location updateExistingLocation(final Location Location) {
        return locationRepository.save(Location);
    }

    @Override
    public String deleteLocation(final Long id) {
        Optional<Location> LocationOptional = locationRepository.findOne(id);
        if (LocationOptional.isPresent()) {
            locationRepository.delete(LocationOptional.get());
            return "User deleted with id " + id;
        } else {
            throw new RuntimeException("User not found for id " + id);
        }
    }
*

}*/
