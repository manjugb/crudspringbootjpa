@get_update_put_post
Feature: user able to update details of customer
Scenario Outline: User able update details of Customer

Given I Enter "<url>" as a Request
When I retrieves the Address "<url>"
Then the status code is "<statuscode>"

Examples:
|url|statuscode|
|http://localhost:8282/api/v1/update?id=1&houseno=12|200|
|http://localhost:8282/api/v1/update?id=2&houseno=15|200|
|http://localhost:8282/api/v1/update?id=3&houseno=12456|200|