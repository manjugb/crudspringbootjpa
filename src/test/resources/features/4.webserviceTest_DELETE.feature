@api_delete_get
Feature: user able to delete details of customer
Scenario Outline: User able update details of Customer

Given I Enter "<url>" as a Request
When I retrieves the Address "<url>"
Then the status code is "<statuscode>"

Examples:
|url|statuscode|
|http://localhost:8282/api/v1/delete?id=1|200|
|http://localhost:8282/api/v1/delete?id=2|200|
|http://localhost:8282/api/v1/delete?id=3|200|