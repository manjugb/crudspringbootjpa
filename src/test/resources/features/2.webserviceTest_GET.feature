@api_get
Feature: user able to get details of customer
Scenario Outline: User able get details of Customer

Given I Enter "<url>" as a Request
When I retrieves the Address "<url>"
Then the status code is "<statuscode>"

Examples: 
|url|statuscode|
|http://localhost:8282/api/v1/read?id=1|200|
|http://localhost:8282/api/v1/read?id=2|200|
|http://localhost:8282/api/v1/read?id=3|200|
