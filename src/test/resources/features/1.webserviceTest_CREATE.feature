@api_post_create
Feature: user able to create customer
Scenario Outline: User able create customer

Given I Enter "<url>" as a Request
When I retrieves the Address "<url>"
Then the status code is "<statuscode>"

Examples:
|url|statuscode|
|http://localhost:8282/api/v1/create?houseno=4/1166&StreetName=NandamuriNagar&PostCode=51500&City=AP&Country=INDIA|200|
|http://localhost:8282/api/v1/create?houseno=6&StreetName=Rue Daunou&PostCode=75002&City=France&Country=France|200|
|http://localhost:8282/api/v1/creat?houseno=7&StreetName=Rue Daunou&PostCode=75002&City=France&Country=France|404|
|http://localhost:8282/api/v/create?houseno=8&StreetName=Rue Daunou&PostCode=75002&City=France&Country=France|404|
|http://localhost:8282/api/v1/create?houseno=9&StreetName=Rue Daunou&PostCode=75002&City=France&Country=France|200|