package options;

import static org.junit.Assert.assertEquals;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.java.jpa.SpringBootJpaSpringDataApplication;
import com.java.jpa.models.Location;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootJpaSpringDataApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LocationControllerIT {

    final Location location = new Location();
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @LocalServerPort
    private int port;

    @Before
    public void before() throws Exception {
    
    	location.setCountry("4");
    	location.setStreetName("Male");
    	location.setPostCode("45345");
    	location.setCity("Fooo");
    	location.setCountry("Fooo");
    }

    @Test
    public void testUserRestEndPoints() throws JSONException {

        testCreateUser();

        testGetUser();

        testDelete();


    }

    private void testCreateUser() {
        //Test create user
        HttpEntity<Location> entity = new HttpEntity<>(location, headers);

        ResponseEntity<Location> response = restTemplate.exchange(
                createURLWithPort("/user/create"),
                HttpMethod.POST, entity, Location.class);
        location.setId(response.getBody().getId());
        assertEquals(location, response.getBody());
    }

    private void testGetUser() {
        //test Get User with id
        HttpEntity<String> entityGet = new HttpEntity<>(null, headers);
        ResponseEntity<Location> responseGet = restTemplate.exchange(
                createURLWithPort("/user/read/" + location.getId()),
                HttpMethod.GET, entityGet, Location.class);

        assertEquals(location, responseGet.getBody());
    }

    private void testDelete() {
        //test delete user with id
        HttpEntity<String> entityDelete = new HttpEntity<>(null, headers);
        ResponseEntity<String> responseDelete = restTemplate.exchange(
                createURLWithPort("/user/delete/" + location.getId()),
                HttpMethod.DELETE, entityDelete, String.class);

        assertEquals("User deleted with id " + location.getId(), responseDelete.getBody());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}